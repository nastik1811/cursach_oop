package com.scr.controller;
import com.scr.Main;
import com.scr.animations.Shake;
import com.scr.dao.StudioDao;
import com.scr.dao.StudioDaoImpl;
import com.scr.model.Studio;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class StudioListViewController {
    @FXML
    private TableView<Studio> studioTbl;

    @FXML
    private TableColumn<Studio, Integer> idColumn;

    @FXML
    private TableColumn<Studio, String> nameColumn;

    @FXML
    private TableColumn<Studio, Double> spaceColumn;

    @FXML
    private TableColumn<Studio, Double> priceColumn;


    @FXML
    private Button updStudioBtn;

    @FXML
    private Button rmStudioBtn;

    @FXML
    void removeStudio(ActionEvent event) {
        Studio studio = studioTbl.getSelectionModel().getSelectedItem();
            if (studio!= null) {
                studioDao.delete(studio);
                studios.remove(studio);
                studioTbl.refresh();
                }
            else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("No Selection");
                alert.setHeaderText("Studio is not selected");
                alert.showAndWait();
            }
    }

    @FXML
    void updateStudio(ActionEvent event) {
    }
    @FXML
    private Button backBtn;

    @FXML
    void goBack(ActionEvent event) throws Exception {
        Shake stdListAnim = new Shake(studioTbl);
        stdListAnim.play();
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }

    @FXML
    private Button addStudioBtn;

    @FXML
    void addStudio(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/NewStudioView.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();
            NewStudioViewController newStudioViewController = loader.getController();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(layout));
            stage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private Main mainApp;
    private StudioDao studioDao = StudioDaoImpl.getInstance();
    private ObservableList<Studio> studios = studioDao.getAll();

    @FXML
    private void initialize() {
        studioTbl.setItems(studios);
        //studentsList.setCellFactory(studentListView -> new StudentViewCellController());
        nameColumn.setCellValueFactory(new PropertyValueFactory<Studio,String>("name"));
        idColumn.setCellValueFactory(new PropertyValueFactory<Studio,Integer>("id"));
        spaceColumn.setCellValueFactory(new PropertyValueFactory<Studio,Double>("square"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<Studio,Double>("pricePerHour"));
        studioTbl.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }
    @FXML
    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
    }
    private void refreshTable(){
        studioTbl.setItems(studioDao.getAll());
        studioTbl.refresh();

    }

}