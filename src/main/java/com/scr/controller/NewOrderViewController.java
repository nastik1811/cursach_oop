package com.scr.controller;

import com.scr.dao.*;
import com.scr.model.Equipment;
import com.scr.model.Order;
import com.scr.model.Studio;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;

public class NewOrderViewController {

    @FXML
    private TextField fNameField;

    @FXML
    private TextField lNameField;

    @FXML
    private TextField emailField;

    @FXML
    private Button createBtn;

    @FXML
    private ListView<Equipment> equipList;

    @FXML
    private Button addEquipBtn;

    @FXML
    private ComboBox<Studio> studioChoice;

    @FXML
    private DatePicker datepicker;

    @FXML
    private Spinner<Integer> hoursPicker;

    @FXML
    void addEquipmentToOrder(ActionEvent event) {

    }

    @FXML
    void createNewOrder(ActionEvent event) {
        if(lNameField.getText().isEmpty() || studioChoice.getSelectionModel().isEmpty()||hoursPicker.getValue().equals(0)){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning!");
            alert.setHeaderText("Can't create order.");
            alert.setContentText("Please fill all fields!");
            alert.showAndWait();
        }
        else {
            Order order = new Order();
            order.setFirstName(fNameField.getText());
            try {

            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warning!");
                alert.setHeaderText("Can't create order.");
                alert.setContentText("You enter wrong data!");
                alert.showAndWait();
            } finally {
            }
        }

    }
    private StudioDao studioDao = StudioDaoImpl.getInstance();
    private EquipmentDao equipmentDao = EquipmentDaoImpl.getInstance();
    private OrderDao orderDao = OrderDaoImpl.getInstance();

    @FXML
    private void initialize() {
        studioChoice.setItems(studioDao.getAll());
        equipList.setItems(equipmentDao.getAll());
    }
}