package com.scr.controller;

import com.scr.dao.StudioDao;
import com.scr.dao.StudioDaoImpl;
import com.scr.model.Studio;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.stage.Stage;

public class NewStudioViewController {

    @FXML
    private TextField nameField;

    @FXML
    private TextField spaceField;

    @FXML
    private TextField priceField;

    @FXML
    private Button createBtn;

    private StudioDao studioDao = StudioDaoImpl.getInstance();

    @FXML
    void createNewStudio(ActionEvent event)  {
        if(nameField.getText().isEmpty() || spaceField.getText().isEmpty() || priceField.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning!");
            alert.setHeaderText("Can't create studio.");
            alert.setContentText("Please fill all fields!");
            alert.showAndWait();
        }
        else{
            Studio s = new Studio();
            s.setName(nameField.getText());
            try{
                s.setSquare(Double.parseDouble(spaceField.getText()));
                s.setPricePerHour(Double.parseDouble(priceField.getText()));
                studioDao.create(s);
            }
            catch (Exception e){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warning!");
                alert.setHeaderText("Can't create studio.");
                alert.setContentText("You enter wrong data!");
                alert.showAndWait();
            }
            finally {
                ((Node)(event.getSource())).getScene().getWindow().hide();
            }

        }
    }



}
