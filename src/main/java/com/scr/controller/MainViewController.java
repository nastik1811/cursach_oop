package com.scr.controller;

import com.scr.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
 public class MainViewController  {
     @FXML
     private Button viewStudioBtn;

     @FXML
     private Button viewEqipBtn;

     @FXML
     private Button viewOredersBtn;

     @FXML
     private Button newOrderBtn;

     @FXML
     void createNewOrder(ActionEvent event) throws IOException {
         mainApp.getPrimaryStage().hide();
         Stage newStage = new Stage();

         FXMLLoader loader = new FXMLLoader();
         loader.setLocation(Main.class.getResource("/view/NewOrderView.fxml"));
         NewOrderViewController newOrderViewController = loader.getController();
         Parent layout =  loader.load();
         Scene scene = new Scene(layout);
         newStage.setScene(scene);
         newStage.show();

     }

     @FXML
     void doToEquipment(ActionEvent event) throws IOException {
         mainApp.getPrimaryStage().hide();
         Stage newStage = new Stage();

         FXMLLoader loader = new FXMLLoader();
         loader.setLocation(Main.class.getResource("/view/EquipmentListView.fxml"));
         EquipmentListViewController equipmentListViewController = loader.getController();
         Parent layout =  loader.load();
         Scene scene = new Scene(layout);
         newStage.setScene(scene);
         newStage.show();
     }

     @FXML
     void goToOrders(ActionEvent event) throws IOException {
         mainApp.getPrimaryStage().hide();
         Stage newStage = new Stage();

         FXMLLoader loader = new FXMLLoader();
         loader.setLocation(Main.class.getResource("/view/OrdersListView.fxml"));
         OrdersListViewController ordersListViewController = loader.getController();
         Parent layout =  loader.load();
         Scene scene = new Scene(layout);
         newStage.setScene(scene);
         newStage.show();

     }

     @FXML
     void goToStudios(ActionEvent event) throws IOException {
         mainApp.getPrimaryStage().hide();
         Stage newStage = new Stage();

         FXMLLoader loader = new FXMLLoader();
         loader.setLocation(Main.class.getResource("/view/StudioListView.fxml"));
         StudioListViewController studioListViewController = loader.getController();
         Parent layout =  loader.load();
         Scene scene = new Scene(layout);
         newStage.initOwner(mainApp.getPrimaryStage());
         newStage.setScene(scene);
         newStage.showAndWait();
     }
     private Main mainApp;

     public void setMainApp(Main mainApp) {
         this.mainApp = mainApp;
     }
 }
