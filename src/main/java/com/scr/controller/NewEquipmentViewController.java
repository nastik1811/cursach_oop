package com.scr.controller;

import com.scr.dao.EquipmentDao;
import com.scr.dao.EquipmentDaoImpl;
import com.scr.dao.StudioDao;
import com.scr.dao.StudioDaoImpl;
import com.scr.model.Equipment;
import com.scr.model.Studio;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class NewEquipmentViewController {

    @FXML
    private TextField nameField;

    @FXML
    private TextField typeField;

    @FXML
    private TextField priceField;

    @FXML
    private Button createBtn;

    private EquipmentDao equipmentDao = EquipmentDaoImpl.getInstance();
    @FXML
    void createNewEquipment(ActionEvent event) {
        if(nameField.getText().isEmpty() || typeField.getText().isEmpty() || priceField.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning!");
            alert.setHeaderText("Can't create equipment.");
            alert.setContentText("Please fill all fields!");
            alert.showAndWait();
        }
        else{
            Equipment s = new Equipment();
            s.setName(nameField.getText());
            s.setType(typeField.getText());
            try{
                s.setPricePerHour(Double.parseDouble(priceField.getText()));
                equipmentDao.create(s);
            }
            catch (Exception e){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warning!");
                alert.setHeaderText("Can't create studio.");
                alert.setContentText("You enter wrong data!");
                alert.showAndWait();
            }
            finally {
                ((Node)(event.getSource())).getScene().getWindow().hide();
            }

        }

    }

}
