package com.scr.controller;

import com.scr.Main;
import com.scr.animations.Shake;
import com.scr.dao.EquipmentDao;
import com.scr.dao.EquipmentDaoImpl;
import com.scr.dao.StudioDao;
import com.scr.dao.StudioDaoImpl;
import com.scr.model.Equipment;
import com.scr.model.Studio;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class EquipmentListViewController {
    @FXML
    private TableView<Equipment> equipTbl;

    @FXML
    private TableColumn<Equipment, Integer> idColumn;

    @FXML
    private TableColumn<Equipment, String> nameColumn;

    @FXML
    private TableColumn<Equipment, String> typeColumn;

    @FXML
    private TableColumn<Equipment, Double> priceColumn;

    @FXML
    private Button addBtn;

    @FXML
    private Button removeBtn;

    @FXML
    private Button backBtn;

    @FXML
    void addEquip(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/NewEquipmentView.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();
            NewEquipmentViewController newEquipmentViewController = loader.getController();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(layout));
            stage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    void goBack(ActionEvent event) {
        Shake eqpListAnim = new Shake(equipTbl);
        eqpListAnim.play();

    }

    @FXML
    void removeEquip(ActionEvent event) {
        Equipment equipment = equipTbl.getSelectionModel().getSelectedItem();
        if (equipment!= null) {
            equipmentDao.delete(equipment);
            all_equipment.remove(equipment);
            equipTbl.refresh();
        }
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("Equipment is not selected");
            alert.showAndWait();
        }
    }

    private Main mainApp;
    private EquipmentDao equipmentDao = EquipmentDaoImpl.getInstance();
    private ObservableList<Equipment> all_equipment = equipmentDao.getAll();

    @FXML
    private void initialize() {
        equipTbl.setItems(all_equipment);
        nameColumn.setCellValueFactory(new PropertyValueFactory<Equipment,String>("name"));
        idColumn.setCellValueFactory(new PropertyValueFactory<Equipment,Integer>("id"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<Equipment,String>("type"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<Equipment,Double>("pricePerHour"));
        equipTbl.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }
    @FXML
    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
    }
}
