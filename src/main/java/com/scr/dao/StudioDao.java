package com.scr.dao;

import com.scr.model.Studio;
import javafx.collections.ObservableList;

import java.util.List;

public interface StudioDao {
    ObservableList<Studio> getAll();
    Studio find(int id);
    Studio find(String name);
    boolean delete(int id);
    boolean delete(Studio studio);
    boolean create(Studio studio);
    boolean update(Studio studio);

}
