package com.scr.dao;

import com.scr.model.Equipment;
import javafx.collections.ObservableList;

public interface EquipmentDao {
    ObservableList<Equipment> getAll();
    Equipment find(int id);
    Equipment find(String name);
    boolean delete(int id);
    boolean delete(Equipment equipment);
    boolean create(Equipment equipment);
    boolean update(Equipment equipment);

}
