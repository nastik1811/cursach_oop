package com.scr.dao;

import com.scr.DBConnector;
import com.scr.model.Equipment;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

public class EquipmentDaoImpl implements EquipmentDao{
    private static volatile EquipmentDao instance;

    private ObservableList<Equipment> equipment;
    private Connection connection = DBConnector.getConnection();

    private EquipmentDaoImpl(){}

    public static synchronized EquipmentDao getInstance() {
        if (instance == null) {
            instance = new EquipmentDaoImpl();
        }
        return instance;
    }

    public ObservableList<Equipment> getAll(){
        ObservableList<Equipment> equipment = FXCollections.observableArrayList();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM equipment");

            while (rs.next()) {
                Equipment s = extractEquipFromResultSet(rs);
                equipment.add(s);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return equipment;
    }

    public Equipment find(int id) {
        return new Equipment();
    }

    public Equipment find(String name) {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM equipment WHERE name=?");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                return extractEquipFromResultSet(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Equipment extractEquipFromResultSet(ResultSet rs) throws SQLException {
        Equipment equipment = new Equipment();
        equipment.setId( rs.getInt("id") );
        equipment.setName( rs.getString("name") );
        equipment.setType(rs.getString("type") );
        equipment.setPricePerHour( rs.getDouble("price") );
        return equipment;
    }

    public boolean delete(int id) {
        int i = 0;
        try {
            Statement st = connection.createStatement();
            i = st.executeUpdate("DELETE FROM equipment WHERE id=" + id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i == 1;
    }

    public boolean delete(Equipment equipment) {
        return delete(equipment.getId());
    }

    public boolean create(Equipment equipment) {
        int i = 0;
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO equipment (name, type, price) VALUES (?,?,?)");
            ps.setString(1, equipment.getName());
            ps.setString(2, equipment.getType());
            ps.setDouble(3, equipment.getPricePerHour());

            i = ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i == 1;
    }

    public boolean update(Equipment equipment) {
        int i = 0;
        try {
            PreparedStatement ps = connection.prepareStatement("UPDATE equipment SET type=?, price=? WHERE id=?");
            ps.setString(2, equipment.getType());
            ps.setDouble(3, equipment.getPricePerHour());

            i = ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i == 1;
    }

}
