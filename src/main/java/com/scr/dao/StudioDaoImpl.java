package com.scr.dao;

import com.scr.DBConnector;
import com.scr.model.Studio;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

public class StudioDaoImpl implements StudioDao {
    private static volatile StudioDao instance;

    private ObservableList<Studio> studios;
    private  Connection connection = DBConnector.getConnection();

    private StudioDaoImpl(){}

    public static synchronized StudioDao getInstance() {
        if (instance == null) {
            instance = new StudioDaoImpl();
        }
        return instance;
    }

    public ObservableList<Studio> getAll(){
        ObservableList<Studio> studios = FXCollections.observableArrayList();
            try {
                ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM studio");

                while (rs.next()) {
                    Studio s = extractStudioFromResultSet(rs);
                    studios.add(s);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        return studios;
    }

    public Studio find(int id) {
        return new Studio();
    }

    public Studio find(String name) {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM studio WHERE name=?");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                return extractStudioFromResultSet(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Studio extractStudioFromResultSet(ResultSet rs) throws SQLException {
        Studio studio = new Studio();
        studio.setId( rs.getInt("id") );
        studio.setName( rs.getString("name") );
        studio.setSquare(rs.getDouble("square") );
        studio.setPricePerHour( rs.getDouble("price") );
        return studio;
    }

    public boolean delete(int id) {
        int i = 0;
        try {
            Statement st = connection.createStatement();
            i = st.executeUpdate("DELETE FROM studio WHERE id=" + id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i == 1;
    }

    public boolean delete(Studio studio) {
        return delete(studio.getId());
    }

    public boolean create(Studio studio) {
        int i = 0;
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO studio (name, square, price) VALUES (?,?,?)");
            ps.setString(1, studio.getName());
            ps.setDouble(2, studio.getSquare());
            ps.setDouble(3, studio.getPricePerHour());

            i = ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i == 1;
    }

    public boolean update(Studio studio) {
        int i = 0;
        try {
            PreparedStatement ps = connection.prepareStatement("UPDATE studio SET square=?, price=? WHERE id=?");
            ps.setString(1, studio.getName());
            ps.setDouble(2, studio.getSquare());
            ps.setDouble(3, studio.getPricePerHour());

            i = ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i == 1;
    }

}
