package com.scr.dao;

import com.scr.model.Order;
import javafx.collections.ObservableList;

public interface OrderDao {
    ObservableList<Order> getAll();
    Order find(int id);
    boolean delete(int id);
    boolean delete(Order order);
    boolean create(Order order);

}
