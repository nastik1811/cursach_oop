package com.scr.dao;

import com.scr.DBConnector;
import com.scr.model.Order;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

public class OrderDaoImpl implements OrderDao {
    private static volatile OrderDao instance;
    private StudioDao studioDao;

    private ObservableList<Order> orders;
    private Connection connection = DBConnector.getConnection();

    private OrderDaoImpl(){ this.studioDao = StudioDaoImpl.getInstance();}

    public static synchronized OrderDao getInstance() {
        if (instance == null) {
            instance = new OrderDaoImpl();
        }
        return instance;
    }

    public ObservableList<Order> getAll(){
        ObservableList<Order> orders = FXCollections.observableArrayList();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM orders ");

            while (rs.next()) {
                Order s = extractOrderFromResultSet(rs);
                orders.add(s);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    public Order find(int id) {
        return new Order();
    }

    public Order find(String name) {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM studio WHERE name=?");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                return extractOrderFromResultSet(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Order extractOrderFromResultSet(ResultSet rs) throws SQLException {
        Order order = new Order();
        order.setOrderNumber( rs.getInt("id") );
        order.setFirstName(rs.getString("firstname"));
        order.setLastName(rs.getString("lastname"));
        order.setEmail(rs.getString("email"));
        order.setCreationDate( rs.getTimestamp("creationdate") );
        order.setBegin( rs.getDate("start") );
        order.setPeriod( rs.getInt("period") );
        order.setStudio(studioDao.find(rs.getInt("studioid")));
        return order;
    }

    public boolean delete(int id) {
        int i = 0;
        try {
            Statement st = connection.createStatement();
            i = st.executeUpdate("DELETE FROM studio WHERE id=" + id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i == 1;
    }

    public boolean delete(Order order) {
        return delete(order.getOrderNumber());
    }

    public boolean create(Order order) {
        int i = 0;
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO orders (firstname, lastname, email, creationdate, start, end, studioid) VALUES (?,?,?,?,?,?,?)");
            ps.setString(1, order.getFirstName());
            ps.setString(2, order.getLastName());
            ps.setString(3, order.getEmail());
            ps.setTimestamp(4, order.getCreationDate());
            ps.setDate(5, order.getBegin());
            ps.setInt(6, order.getPeriod());
            ps.setInt(7, order.getStudio().getId());
            i = ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i == 1;
    }
}
