package com.scr;

import com.scr.controller.MainViewController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {
    private Stage primaryStage;
    private Parent rootLayout;

    @Override
    public void start(Stage stage) throws Exception{
        DBConnector.getInstance().initialize();
        this.primaryStage = stage;
        this.primaryStage.setTitle("Studio manager");

        initRootLayout();
    }

    public void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/view/MainView.fxml"));
            rootLayout = (Parent) loader.load();
            MainViewController mainViewController = loader.getController();
            mainViewController.setMainApp(this);

            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Stage getPrimaryStage() {
        return primaryStage;
    }
    public Parent getRootLayout(){return rootLayout;}

    public static void main(String[] args) {
        launch(args);
    }
}
