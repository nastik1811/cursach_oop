package com.scr.animations;

import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.util.Duration;

public class Shake {
    private TranslateTransition tt;

    public Shake(Node node){
        tt = new TranslateTransition(Duration.millis(80), node);
        tt.setByX(20f);
        tt.setByX(-20f);
        tt.setCycleCount(4);
        tt.setAutoReverse(true);
    }
    public void play(){
        tt.playFromStart();
    }
}
