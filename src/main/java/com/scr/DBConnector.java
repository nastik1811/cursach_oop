package com.scr;

import com.scr.dao.StudioDao;
import com.scr.dao.StudioDaoImpl;
import com.scr.model.Studio;

import java.sql.*;
import java.util.ArrayList;

public class DBConnector {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/stdb";
    private static final String user = "root";
    private static final String password = "111";

    private static volatile DBConnector instance;
    public StudioDao studioDao;

    private Connection connection;
    private Statement statement;

    public static boolean isAvailable = true;

    private static final String INSERT_COMMAND = "INSERT INTO";
    private static final String SELECT_COMMAND = "";
    private static final String REMOVE_COMMAND = "DELETE FROM Registration WHERE uuid = ";


    private DBConnector() {
        try {
            connection = DriverManager.getConnection(DB_URL, user, password);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static DBConnector getInstance(){
        if (instance == null) {
            instance = new DBConnector();
        }
        return instance;
    }

    public static Connection getConnection(){
        if (instance == null) {
            instance = new DBConnector();
        }
        return instance.connection;
    }
    public void initialize() throws SQLException {
        isAvailable = false;

        studioDao = StudioDaoImpl.getInstance();

        isAvailable = true;
    }

}
