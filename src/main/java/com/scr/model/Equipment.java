package com.scr.model;

import java.io.Serializable;

public class Equipment implements Serializable {
    private int id;

    private String name;
    private String type;
    private Double price;


    public Equipment() {}
    public Equipment(String name, String type, Double price, int id) {
        this.name = name;
        this.type = type;
        this.price = price;
        this.id = id;
    }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getPricePerHour() { return price; }
    public void setPricePerHour(Double price) { this.price = price; }
    @Override
    public String toString() {
        return id + ". " + name + " (" + type + ") ";
    }

}
