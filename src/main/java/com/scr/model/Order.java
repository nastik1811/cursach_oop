package com.scr.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.sql.Date;
import java.util.List;

public class Order implements Serializable {

    private int orderNumber;

    private String firstName;
    private String lastName;
    private String email;

    private Timestamp creationDate;

    private Date begin;
    private int period;

    private Studio studio;
    private Status status;
    private List<Equipment> equipment;

    public enum Status {
        FAILED,
        APPROVED,
        CANCELLED,
    }

    public Order() {}

    public Order(int orderNumber, String firstName, String lastName, String email, Timestamp creationDate,
                 Date begin, int period, Studio studio) {
        this.orderNumber = orderNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.creationDate = creationDate;
        this.begin = begin;
        this.period = period;
        this.studio = studio;

    }
    public int getOrderNumber() { return orderNumber; }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getBegin(){return begin;}

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }
    public Studio getStudio(){return studio;}
    public void setStudio(Studio studio){this.studio = studio;}

    public Timestamp getCreationDate() { return creationDate; }
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
    @Override
    public String toString() {
        return orderNumber + ". Customer: " + firstName + " " + lastName + " creation date: " + creationDate + " status:" + status.name();
    }
}
