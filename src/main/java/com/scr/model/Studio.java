package com.scr.model;


import java.io.Serializable;

public class Studio implements Serializable {
    private int id;
    private String name;
    private double square;
    private double pricePerHour;


    public Studio() {}
    public Studio(int id, String name, double square, double pricePerHour) {
        this.name = name;
        this.square = square;
        this.pricePerHour = pricePerHour;
        this.id = id;
    }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public double getSquare() { return square; }
    public void setSquare(double square) { this.square = square; }

    public double getPricePerHour() { return pricePerHour; }
    public void setPricePerHour(double pricePerHour) { this.pricePerHour = pricePerHour; }

    @Override
    public String toString() {
        return id + ". " + name;
    }


}